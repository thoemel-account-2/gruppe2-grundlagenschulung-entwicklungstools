package ch.ergon.edu.ci.mailinator.domain;

public class Letter {
    private String name;
    private String street;
    private int plz;
    private String city;

    private String content;
    private String signature;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public static class Builder {
        private final Letter letter = new Letter();

        public Builder name(String name) {
            letter.setName(name);
            return this;
        }

        public Builder content(String content) {
            letter.setContent(content);
            return this;
        }

        public Builder city(String city) {
            letter.setCity(city);
            return this;
        }

        public Builder plz(int plz) {
            letter.setPlz(plz);
            return this;
        }

        public Builder signature(String signature) {
            letter.setSignature(signature);
            return this;
        }

        public Letter build() {
            return letter;
        }
    }

    @Override
    public String toString() {
        return "Letter{"
                + "name='" + name + '\''
                + ", street='" + street + '\''
                + ", plz=" + plz
                + ", city='" + city + '\''
                + ", content='" + content + '\''
                + ", signature='" + signature + '\''
                + '}';
    }
}
