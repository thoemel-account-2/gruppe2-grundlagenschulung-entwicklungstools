package ch.ergon.edu.ci.mailinator;

import ch.ergon.edu.ci.mailinator.distribution.AddressGenerator;
import ch.ergon.edu.ci.mailinator.distribution.ContentGenerator;
import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;
import ch.ergon.edu.ci.mailinator.validation.LetterValidator;

import java.util.ArrayList;
import java.util.List;

public class Mailinator {

    private ContentGenerator contentGenerator = new ContentGenerator();
    private LetterValidator letterValidator = new LetterValidator();

    public List<Letter> prepareMailing(List<AddressEntry> addressEntryList) {
        List<Letter> letters = new ArrayList<>();
        for (AddressEntry addressEntry : addressEntryList) {
            Letter letter = generateLetter(addressEntry);
            if (letterValidator.isValid(letter)) {
                letters.add(letter);
            }
        }
        return letters;
    }

    private Letter generateLetter(AddressEntry addressEntry) {
        Letter letter = new Letter();
        AddressGenerator.insertAddress(letter, addressEntry);
        contentGenerator.insertContent(letter);
        return letter;
    }

}
