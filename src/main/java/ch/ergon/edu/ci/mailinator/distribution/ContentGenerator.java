package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;

import java.util.Random;

public class ContentGenerator {
    private static final int CONTENT_LENGTH = 128;
    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "abcdefghijklmnopqrstuvwxyz";

    public void insertContent(Letter letter) {
        Random random = new Random();
        String content;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < CONTENT_LENGTH; i++) {
            stringBuffer.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }
        content = stringBuffer.toString();
        letter.setContent(content);
    }
}
