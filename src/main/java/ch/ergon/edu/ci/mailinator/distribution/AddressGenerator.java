package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;

public final class AddressGenerator {
    private AddressGenerator() {

    }
    public static void insertAddress(Letter letter, AddressEntry addressEntry) {
        letter.setCity(addressEntry.getCity());
        letter.setName(separateWithWhitespace(addressEntry.getFirstName(), addressEntry.getLastName()));
        letter.setStreet(separateWithWhitespace(addressEntry.getStreetName(), addressEntry.getHouseNumber()));
        letter.setPlz(Integer.parseInt(addressEntry.getPlz()));
    }

    private static String separateWithWhitespace(String firstString, String secondString) {
        return String.format("%s %s", firstString, secondString);
    }
}
