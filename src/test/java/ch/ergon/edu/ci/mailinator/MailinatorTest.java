package ch.ergon.edu.ci.mailinator;

import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;
import java.util.List;

import static ch.ergon.edu.ci.mailinator.ContainsLetterWithNameMatcher.containsLetterWithName;

public class MailinatorTest {

    private Mailinator mailinator = new Mailinator();

    @Category(IntegrationTest.class)

    @Test
    public void onlyCorrectLetters() {
        AddressEntry valid1 = new AddressEntry.Builder()
                .firstName("F1").lastName("L1").streetName("S1").houseNumber("1").plz("1111").city("Lausanne").build();
        AddressEntry valid2 = new AddressEntry.Builder()
                .firstName("F2").lastName("L2").streetName("S2").houseNumber("2").plz("2222").city("Freiburg").build();
        AddressEntry valid3 = new AddressEntry.Builder()
                .firstName("F3").lastName("L3").streetName("S3").houseNumber("3").plz("3333").city("Bern").build();
        List<AddressEntry> addressList = Arrays.asList(valid1, valid2, valid3);

        List<Letter> letters = mailinator.prepareMailing(addressList);

        Assert.assertThat(letters, Matchers.hasSize(addressList.size()));
        Assert.assertThat(letters, containsLetterWithName("F1 L1"));
        Assert.assertThat(letters, containsLetterWithName("F2 L2"));
        Assert.assertThat(letters, containsLetterWithName("F3 L3"));
    }

    @Category(IntegrationTest.class)
    
    @Test
    public void incorrectLetterIsNotPrepared() {
        AddressEntry invalid1 = new AddressEntry.Builder()
                .firstName("F1").lastName("L1").streetName("S1").houseNumber("1").plz("1111").city("Nirgends").build();
        AddressEntry invalid2 = new AddressEntry.Builder()
                .firstName("F2").lastName("L2").streetName("S2").houseNumber("2").plz("22222").city("Freiburg").build();
        AddressEntry valid = new AddressEntry.Builder()
                .firstName("F3").lastName("L3").streetName("S3").houseNumber("3").plz("3333").city("Bern").build();

        List<Letter> letters = mailinator.prepareMailing(Arrays.asList(invalid1, invalid2, valid));

        Assert.assertThat(letters, Matchers.hasSize(1));
        Assert.assertThat(letters, Matchers.not(containsLetterWithName("F1 L1")));
        Assert.assertThat(letters, Matchers.not(containsLetterWithName("F2 L2")));
        Assert.assertThat(letters, containsLetterWithName("F3 L3"));
    }


}

final class ContainsLetterWithNameMatcher extends TypeSafeMatcher<List<Letter>> {
    private Letter letterToFind;

    private ContainsLetterWithNameMatcher(Letter letter) {
        letterToFind = letter;
    }

    static ContainsLetterWithNameMatcher containsLetterWithName(String name) {
        return new ContainsLetterWithNameMatcher(new Letter.Builder().name(name).build());
    }

    @Override
    protected boolean matchesSafely(List<Letter> letters) {
        boolean matches = false;
        for (Letter letter : letters) {
            if (letter.getName().equals(letterToFind.getName())) {
                matches = true;
            }
        }
        return matches;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Name " + letterToFind.getName() + " is found in list");

    }
}
