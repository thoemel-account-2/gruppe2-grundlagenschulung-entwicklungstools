package ch.ergon.edu.ci.mailinator.validation;

import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class LetterValidatorTest {
    private static final int VALID_PLZ = 8400;
    private static final int INVALID_PLZ = 12345;
    public static final int ZH_PLZ = 8021;
    private final LetterValidator letterValidator = new LetterValidator();

    /**
     * Unit Test zu Aufgabe 3
     */
    @Test
    public void validPlz() {
        boolean valid = letterValidator.isPlzFormatCorrect(VALID_PLZ);
        Assert.assertThat(valid, Matchers.is(true));
    }

    /**
     * Unit Test zu Aufgabe 3
     */
    @Test
    public void invalidPlz() {
        boolean valid = letterValidator.isPlzFormatCorrect(INVALID_PLZ);
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 4
     */
    @Test
    public void validContent() {
        boolean valid = letterValidator.isContentNotEmpty("abc");
        Assert.assertThat(valid, Matchers.is(true));
    }

    /**
     * Unit Test zu Aufgabe 4
     */
    @Test
    public void emptyContentIsInvalid() {
        boolean valid = letterValidator.isContentNotEmpty("");
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 4
     */
    @Test
    public void nullContentIsInvalid() {
        boolean valid = letterValidator.isContentNotEmpty(null);
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Test
    public void validCity() {
        boolean valid = letterValidator.isValidCity("Neuenburg");
        Assert.assertThat(valid, Matchers.is(true));
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Test
    public void invalidCity() {
        boolean valid = letterValidator.isValidCity("Hinterpfupfikon");
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Test
    public void invalidEmptyCity() {
        boolean valid = letterValidator.isValidCity("");
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 5
     */
    @Test
    public void invalidNullCity() {
        boolean valid = letterValidator.isValidCity(null);
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Test
    public void validLetter() {
        Letter letter = new Letter.Builder().content("gültiger Content").city("Zürich").plz(ZH_PLZ).build();
        boolean valid = letterValidator.isValid(letter);
        Assert.assertThat(valid, Matchers.is(true));
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Test
    public void invalidLetterBecauseOfContent() {
        Letter letter = new Letter.Builder().content("").city("Zürich").plz(ZH_PLZ).build();
        boolean valid = letterValidator.isValid(letter);
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Test
    public void invalidLetterBecauseOfCity() {
        Letter letter = new Letter.Builder().content("gültiger Content").city("Nirgendwo").plz(ZH_PLZ).build();
        boolean valid = letterValidator.isValid(letter);
        Assert.assertThat(valid, Matchers.is(false));
    }

    /**
     * Unit Test zu Aufgabe 6
     */
    @Test
    public void invalidLetterBecauseOfPlz() {
        Letter letter = new Letter.Builder().content("gültiger Content").city("Zürich").plz(0).build();
        boolean valid = letterValidator.isValid(letter);
        Assert.assertThat(valid, Matchers.is(false));
    }

}
